import { Component } from "react";

class CountClick extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }

        // Cách 1: Sử dụng bind trỏ this của function về class
        // this.clickChangeHandler = this.clickChangeHandler.bind(this);
    }

    // clickChangeHandler() {
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }

    // Cách 2: Khai báo thuộc tính = arrow function
    clickChangeHandler = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.clickChangeHandler}>Click here</button>
            </div>
        )
    }
}

export default CountClick;